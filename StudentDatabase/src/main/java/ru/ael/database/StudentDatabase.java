
package ru.ael.database;

import ru.ael.database.student.Student;


public class StudentDatabase {

    /**
     * Ячейка памяти
     */
    private static String group = "ПИ-21 (б)";
    
    /**
     * Стартовый метод со стандартным именем main
     * 
     * @param args 
     */
    public static void main(String[] args) {
        
        System.out.println("первая программа...");
        System.out.println("Группа: "+group);
        System.out.println("прошел гд обучения...");
        
        group = "ПИ-31 (б)";
        System.out.println("Группа: "+group);
        System.out.println("Создание экземпляров класса Student ...");
        
        Student s1 = new Student();
        s1.setИмя("Иван"); s1.setФамилия("Иванов");        
        s1.setОтчество("Иванович");
        
        System.out.println(s1.toString());
        
        Student student = new Student();
        
        System.out.println(student.toString());
        
        System.out.println("Создание экземпляров класса Student ЗАВЕРШЕНО");
        
    }
}
