/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.database.student;

/**
 * Класс для предсавления экземпляров студента
 * 
 * @author developer
 */

public class Student {
    
    /**
     * Строковое представление поля фамилии
     */
    private String Фамилия;
    private String Имя;    
    private String Отчество;

    @Override
    public String toString() {
        return "Student{" + "\u0424\u0430\u043c\u0438\u043b\u0438\u044f=" + Фамилия + ", \u0418\u043c\u044f=" + Имя + ", \u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e=" + Отчество + '}';
    }

    
    public String getОтчество() {
        return Отчество;
    }

    
    public void setОтчество(String Отчество) {
        this.Отчество = Отчество;
    }


   
    public String getИмя() {
        return Имя;
    }

   
    public void setИмя(String Имя) {
        this.Имя = Имя;
    }

    
    
    /**
     * Прочитать поле Фамилия
     *
     * @return the value of Фамилия
     */
    public String getФамилия() {
        return Фамилия;
    }

    /**
     * Записать поле Фамилия
     *
     * @param Фамилия new value of Фамилия
     */
    public void setФамилия(String Фамилия) {
        this.Фамилия = Фамилия;
    }

    
}

